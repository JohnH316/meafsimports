﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MEAFSImport;
using MEAFSDLL;

namespace MEAFSImport.Views.Imports
{
    /// <summary>
    /// Summary description for FileImport
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class FileImport : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public string Import(string process, string Directory)
        {
            var importFile = new MEAFSDLL.ImportRoutine();
            string ImportResults = importFile.Imports(Directory,process);
            return ImportResults;
        }
    }
}
