﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEAFSImport.Models
{
    public class SystemConstants
    {
        public string BaseDirectory { get; set; }
        public string ServiceParams { get; set; }
    }
}