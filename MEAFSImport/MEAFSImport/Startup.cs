﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MEAFSImport.Startup))]
namespace MEAFSImport
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
