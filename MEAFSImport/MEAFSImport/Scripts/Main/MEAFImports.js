﻿$(function () {
    $("#TxtInfo").hide();
    $(document).on('click', '[name="GoSubmit"]', function () {

        var e = document.getElementById("SelectedId");
        var DDLPick = e.options[e.selectedIndex].text;
        //This should pass report and params... JHH
        //var File = "GLIS-0234-MADISON-MEA_150508_201856.TXT";
        if (DDLPick == "--Please Select--")
        {
            $("#TxtInfo").show();
            $("#TxtInfo").text("Please select a choice from the drop down list");
        }
        else
        {
            if (DDLPick == "Run Current Month/Year")
            {
                DDLPick = "";
            }
        $.ajax({
            type: "POST",
            url: "WebServices/FileImport.asmx/Import ",
            data: "{'process':'MANUAL','Directory':'" + DDLPick + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#TxtInfo").text("");
                $("#TxtInfo").show();
                $("#TxtInfo").text(data.d);
            },   //end of success
            error: function (xhr, status, error) {
                alert(error.toString());
            }  // end of error
        });        // end of ajax
    }
    });
});