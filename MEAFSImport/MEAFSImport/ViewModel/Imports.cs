﻿using System.Collections.Generic;
using System.Web.Mvc;
namespace MEAFSImport.ViewModel
{

    public class DirectoryListType
    {
        public int Value { get; set; }
        public string Type { get; set; }
    }

    public class FileDirectoryResult
    {
        public string SelectedId { get; set; }
        public SelectList FileSelectResults { get; set; }
    }
    //public class FileDirectories
    //{
    //    public List<FileDirectoryResult> FileDirectoryResults { get; set; }
        
    //}
}
