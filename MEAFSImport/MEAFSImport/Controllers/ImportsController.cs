﻿using MEAFSEntity;
using MEAFSImport.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;  

namespace MEAFSImport.Controllers
{
    public class ImportsController : Controller
    {
        
        // GET: Imports
        private MEAFSEntity.MEAFSEntity  db = new MEAFSEntity.MEAFSEntity();
        public ActionResult Index()
        {
            string basedir = (from b in db.SystemConstants
                              select b.BaseDirectory).FirstOrDefault().ToString();
            var vc = new List<DirectoryListType>();
            vc.Add(new DirectoryListType { Type = "--Please Select--", Value = -100 });
            vc.Add(new DirectoryListType { Type = "Run Current Month/Year", Value = -99 });
            DirectoryInfo directory = new DirectoryInfo(basedir.Trim());
            Int32 Cntr = 0;
            // validate param
            if (directory == null) return null;

            // create a node for this directory
            string DirNode = directory.Name;

            // get subdirectories of the current directory
            System.IO.DirectoryInfo[] SubDirectories = directory.GetDirectories();

            // output each subdirectory
            for (int DirectoryCount = 0; DirectoryCount < SubDirectories.Length; DirectoryCount++)
            {
                DirNode = SubDirectories[DirectoryCount].Name;
                directory = new DirectoryInfo(basedir.Trim() + "\\" + SubDirectories[DirectoryCount].Name);
                System.IO.DirectoryInfo[] SubSubDirectories = directory.GetDirectories();

                // output each subdirectory
                for (int SubDirectoryCount = 0; SubDirectoryCount < SubSubDirectories.Length; SubDirectoryCount++)
                {
                    vc.Add(new DirectoryListType { Type = DirNode.ToString() + "/" + SubSubDirectories[SubDirectoryCount].Name.ToString(), Value = Cntr });
                }

            }
             FileDirectoryResult model = new FileDirectoryResult()
              {
                FileSelectResults = new SelectList(vc, "Type", "Value")};
            return View(model);
        }
        public ActionResult Default()
        {
            return View();
        }
    }
}
