﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MEAFSEntity;
using MEAFSImport.Models;

namespace MEAFSImport.Controllers
{
    public class SystemConstantsController : Controller
    {
        private MEAFSEntity.MEAFSEntity db = new MEAFSEntity.MEAFSEntity();
        // GET: SystemConstants
        public ActionResult Index()
        {
            return View();
        }

        // GET: SystemConstants/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: SystemConstants/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SystemConstants/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: SystemConstants/Edit/5
        public ActionResult Edit()
        {

            return View(db.SystemConstants);
        }

        // POST: SystemConstants/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: SystemConstants/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: SystemConstants/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
