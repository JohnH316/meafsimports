﻿using System ;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Routing;

namespace MEAFSImport
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
            name: "FileImports",
            routeTemplate: "api/{controller}/{action}/{File}",
            defaults: new { File = RouteParameter.Optional });
 
            config.Routes.MapHttpRoute(
            name: "RegFileImports",
           routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional });

            //config.Routes.MapHttpRoute(
            //    name: "DefaultStandard",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional });

        }
    }
}
