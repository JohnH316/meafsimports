﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MEAFSDLL.Models
{
    public class FileImportResults
    {
        public string FileCode { get; set; }
        public string FileName { get; set; }
    }
}
