﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using MEAFSDLL.Models;
using System.Data.OleDb;
using System.Data;
using System.Reflection;
using MEAFSEntity;
using System.Data.Entity;

namespace MEAFSDLL
{
    public class ImportRoutine
    {
        private MEAFSEntities db = new MEAFSEntities();
        static void Main(string[] args)
        {
        }
        public string Imports(string MyDirectory, string processType)
        {
            int fileStatus = 0;
            int fileCntr = 0;
            if ((MyDirectory == "--Current Month/Year--") || (MyDirectory == ""))
            {
                DateTime NowDate = DateTime.Now.Date;
                var Year = NowDate.Year;
                var Month = NowDate.Month.ToString()+" "+NowDate.ToString("MMMM");
                MyDirectory = Year.ToString() + "/" + Month.ToString();
                var BegYear = NowDate.Year;
                var EndYear = BegYear;
                var BegMonth = NowDate.Month;
                var NextMonth = BegMonth + 1;
                if (BegMonth == 12)
                {
                    NextMonth = 1;
                    EndYear = EndYear + 1;
                }
                DateTime BegAccountDate = DateTime.Parse(BegMonth.ToString() + "/01/" + BegYear);
                DateTime EndAccountDate = DateTime.Parse(NextMonth.ToString() + "/01/" + EndYear);
            }
            else
            {

                var BegYear = MyDirectory.Substring(0, 4);
                var EndYear = BegYear;
                var BegMonth = MyDirectory.Substring(5, 2);
                var NextMonth = Int32.Parse(BegMonth) + 1;
                if (BegMonth == "12")
                {
                    NextMonth = 1;
                    EndYear = EndYear + 1;
                }
                DateTime BegAccountDate = DateTime.Parse(BegMonth.Trim().ToString() + "/01/" + BegYear);
                DateTime EndAccountDate = DateTime.Parse(NextMonth.ToString() + "/01/" + EndYear);
             }
            string basedir = (from b in db.SystemConstants
                              select b.BaseDirectory).FirstOrDefault();
            string[] filearray = Directory.GetFiles(basedir.Trim()+"/"+MyDirectory);
            FileImportResults FileImportResults = new FileImportResults();
            int fileloop = filearray.Length;
            if (fileloop > 0)
            {
                for (int x = 0; x < fileloop; x++)
                {
                    //Records exist??
                    var MyFile = Path.GetFileName(filearray[x]);
                    var Details = (from b in db.AccountDatas
                                   where b.FileName == MyFile
                                   select b).Count();
                    if (Details > 0)
                    {
                        var Records = db.AccountDatas.Where(u => u.FileName == MyFile);

                        foreach (var u in Records)
                        {
                            db.AccountDatas.Remove(u);
                        }

                        db.SaveChanges();
                    }
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    string filetype = Path.GetExtension(filearray[x]);
                    fileStatus = ImportFile(processType, MyDirectory, filearray[x], filetype);
                    fileCntr = fileCntr + fileStatus;  //Returns 1 Good File or 0 File / errors
                }//FileArray Loop

            }//end if
            //else
            //{
            //FileImportResults.FileName = "No Files found";
            //}
            return (fileloop.ToString() + " files found. " + fileCntr.ToString() + " files successfully uploaded.");
        }//end of Function

        public int ImportFile(string processType, string folder, string filename, string filetype)
        {
            AccountData Account = new AccountData();
            LoanData Loan = new LoanData();
            FileImportResults FileImportResults = new FileImportResults();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            var ExtFileName = Path.GetFileName(filename);
            Int32 numbRecs = 0;
            string ErrMsg = "";
            int counter = 0;
            int GoodFile = 1;
            String[] ColumnNames = { "EMPLOYER EIN", "VENDOR EIN", "VENDOR SOURCE NAME", "AGGREGATOR SOURCE NAME", "VENDOR SOURCE ID", "AGGREGATOR PLAN ID", "VENDOR PLAN ID", "EMPLOYER PLAN ID", "EMPLOYEE ACCOUNT #", "EMPLOYEE SSN", 
                                         "EMPLOYEE ID", "FIRST NAME", "LAST NAME", "DATE OF BIRTH", "CASH VALUE TYPE", "EMPLOYER CASH VALUE", "EE DEFERRAL CASH VALUE", "ROLLOVER EE PRETAX CASH VALUE", "ROLLOVER EE POSTTAX CASH VALUE", 
                                         "ROLLOVER ROTH CASH VALUE", "EE POSTTAX CASH VALUE", "ROTH 403B CASH VALUE", "DATE OF FIRST ROTH CONTRIBUTION", "403B7 CASH VALUE", "CASH VALUE DATE", "TYPE OF ACCOUNT", 
                                         "YEAR TO DATE EE CONTRIBUTIONS", "YEAR TO DATE ER CONTRIBUTIONS", "ACCOUNT INCEPTION TO DATE EE CONTRIBUTIONS", "ACCOUNT INCEPTION TO DATE 15YR CATCH UP CONTRIBUTIONS", "12/31/86 CASH VALUE EE", 
                                         "12/31/86 CASH VALUE ER", "METHOD OF REPORTING HARDSHIP DATA", "TOTAL HARDSHIP AMOUNT AVAILABLE", "LATEST HARDSHIP DISTRIBUTION TYPE", "LATEST HARDSHIP DISTRIBUTION DATE", "LATEST HARDSHIP DISTRIBUTION AMOUNT", 
                                         "HARDSHIP COMPONENT A 12/31/88 CASH VALUE EE", "HARDSHIP COMPONENT B - 12/31/88 CASH VALUE ER", "HARDSHIP COMPONENT C - POST 12/31/88 CONTRIBUTIONS EE", "HARDSHIP COMPONENT D - POST 12/31/88 WITHDRAWALS", 
                                         "ISSUE DATE", "12/31/88 CASH VALUE EE", "METHOD OF FREPORTING LOAN DATA", "MAXIMUM LOAN AMOUNT AVAILABLE", "SEPARATION FROM SERVICE DATE", "NUMBER OF LOANS OUTSTANDING", "PRODUCT ID", "LOAN DEFAULT LOAN INDICATOR", 
                                         "NUMBER OF SETS OF LOAN COMPONENT DATA REPORTED", "VENDOR LOAN NUMBER", "LOAN INITIATION DATE", "LOAN STATUS", "LOAN TYPE INDICATOR", "ORIGINAL LOAN AMOUNT", "REMAINING LOAN BALANCE", "REMAINING BALANCE DATE", 
                                         "HIGHEST OUTSTANDING LOAN BALANCE 12 MONTHS"};
            if (filetype.ToUpper() == ".TXT")
            {
                char[] delimiterChars = { '|' };
                numbRecs = 0;
                try
                {   // Open the text file using a stream reader.
                    using (StreamReader sr = new StreamReader(filename))
                    {
                        while (!sr.EndOfStream)
                        {
                            // Read the stream to a string, and write the string to the console.
                            String line = sr.ReadLine();
                            numbRecs = numbRecs + 1;
                            string[] items;
                            if ((line.Substring(0, 6) != "HEADER") && (line.Substring(0, 7) != "TRAILER"))
                            {
                                items = line.Split(delimiterChars);
                                counter = counter + 1;
                                ErrMsg = Save(processType, folder, items, ExtFileName);
                                if (ErrMsg != "")
                                {
                                    sb.AppendLine(ErrMsg);
                                }
                                Int32 NoLoans = Convert.ToInt32(items[49]);
                                if (NoLoans > 0)
                                {
                                    DateTime NowDate = DateTime.Now.Date;
                                    var EmpID = items[10];
                                    var EmpAccount = items[8];


                                    var EmpEIN = items[0];

                                    var Details = (from b in db.AccountDatas
                                                   where b.EMPLOYEE_ID == EmpID
                                                     && b.EMPLOYEE_ACCOUNT_NO == EmpAccount
                                                     && b.EMPLOYER_EIN == EmpEIN
                                                     && b.FileName == ExtFileName
                                                   select b.ACCOUNTDATAID).FirstOrDefault();
                                    for (int i = 0; i < NoLoans; i++)
                                    {
                                        //Grab the newly created record ID //
                                        Loan.AccountDataID = Convert.ToInt32(Details);
                                        Loan.VENDOR_LOAN_NUMBER = CheckNull(ColumnNames[50], items[50 + ((i) * 8)].ToString(), ExtFileName, processType, folder);
                                        Loan.LOAN_INITIATION_DATE = CheckNull(ColumnNames[51], items[51 + ((i) * 8)].ToString(), ExtFileName, processType, folder);
                                        Loan.LOAN_STATUS = CheckNull(ColumnNames[52], items[52 + ((i) * 8)].ToString(), ExtFileName, processType, folder);
                                        Loan.LOAN_TYPE_INDICATOR = CheckNull(ColumnNames[53], items[53 + ((i) * 8)].ToString(), ExtFileName, processType, folder);
                                        Loan.ORIGINAL_LOAN_AMOUNT = CheckDecimal(ColumnNames[54], items[54 + ((i) * 8)], ExtFileName, processType, folder);
                                        Loan.REMAINING_LOAN_BALANCE = CheckDecimal(ColumnNames[55], items[55 + ((i) * 8)], ExtFileName, processType, folder);
                                        Loan.REMAINING_BALANCE_DATE = CheckNull(ColumnNames[56], items[56 + ((i) * 8)].ToString(), ExtFileName, processType, folder);
                                        Loan.HIGHEST_OUTSTANDING_LOAN_BALANCE_12_MONTHS = CheckDecimal(ColumnNames[57], items[57 + ((i) * 8)], ExtFileName, processType, folder);
                                        SaveLoan(Loan);
                                    }//for
                                }//end if
                            }//end if
                        } // while
                    } //using
                } //try
                catch (Exception e)
                {

                    sb.AppendLine(e.Message.ToString());
                } //catch
            } // if
            else if (filetype.ToUpper() == ".XLS")
            {
                numbRecs = 0;
                FileImportResults.FileName = filename;
                try
                {
                    string datasource1 = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + filename + "; Extended Properties='Excel 12.0;HDR=NO;IMEX=1;'";

                    using (OleDbConnection conn = new OleDbConnection(datasource1))
                    {
                        conn.Open();
                        OleDbCommand cmd = new OleDbCommand();
                        cmd.Connection = conn;
                        DataSet ds = new DataSet();
                        // Get all Sheets in Excel File
                        DataTable dtSheet = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                        // Loop through all Sheets to get data
                        foreach (DataRow dr in dtSheet.Rows)
                        {
                            string sheetName = dr["TABLE_NAME"].ToString();

                            if (!sheetName.EndsWith("$"))
                                continue;

                            // Get all rows from the Sheet
                            cmd.CommandText = "SELECT * FROM [" + sheetName + "]";

                            DataTable dt = new DataTable();
                            dt.TableName = sheetName;

                            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                            da.Fill(dt);
                            ds.Tables.Add(dt);
                        }

                        cmd = null;
                        conn.Close();
                        //
                        var RowStart = 0;
                        var ColStart = 0;
                        numbRecs = ds.Tables[0].Rows.Count;
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            for (int j = 0; j < ds.Tables[0].Rows[i].ItemArray.Count(); j++)
                            {
                                if (ds.Tables[0].Rows[i].ItemArray[j].ToString().ToUpper() == ColumnNames[0])
                                {
                                    RowStart = i;
                                    ColStart = j;
                                    i = ds.Tables[0].Rows.Count;
                                    break;
                                }  //Find Proper Headings
                            }   //j
                        }   //i

                        //Now let's check to make sure fields exist (want to do this once as if we do it in the main loop it'll show for every record.
                        int k = 0;

                        for (int k1 = 0; k1 < 50; k1++)
                        {
                            int foundCol = 0;
                            for (int j = ColStart; j < ds.Tables[0].Rows[RowStart + 1].ItemArray.Count(); j++)
                            {
                                if (ds.Tables[0].Rows[RowStart].ItemArray[j].ToString() != "")
                                {
                                    //Load Data here.
                                    if (ds.Tables[0].Rows[RowStart].ItemArray[j].ToString().ToUpper() == ColumnNames[k1])
                                    {
                                        foundCol = j;
                                    }

                                }//endif
                            } //j
                            if (foundCol == 0)
                            {
                                sb.AppendLine(" Missing Column:" + ColumnNames[k1]);
                                foundCol = 0;
                            }
                        } //k1

                        string[] items = new string[100];
                        k = 0;
                        //ColArray = 0;

                        for (int i = RowStart + 1; i < ds.Tables[0].Rows.Count; i++) //Record Row
                        {
                            for (int j = ColStart; j < ds.Tables[0].Rows[i].ItemArray.Count(); j++) //Field Loop
                            {
                                for (int k1 = 0; k1 < ds.Tables[0].Rows[i].ItemArray.Count(); k1++) //Field Name Loop
                                {
                                    if (ds.Tables[0].Rows[RowStart].ItemArray[j].ToString() != "")
                                    {
                                        //Load Data here.
                                        if (ds.Tables[0].Rows[RowStart].ItemArray[j].ToString().ToUpper() == ColumnNames[k1])
                                        {
                                            items[k] = ds.Tables[0].Rows[i].ItemArray[j].ToString();
                                            k = k + 1;
                                            k1 = ds.Tables[0].Rows[i].ItemArray.Count(); //I found one. no need to go any further. Next Field
                                        }//end if 

                                    }//endif
                                } //k1
                            } //j
                            counter = counter + 1;

                            ErrMsg = Save(processType, folder, items, ExtFileName);
                            if (ErrMsg != "")
                            {
                                sb.AppendLine(ErrMsg);
                                GoodFile = 0;
                            }
                            items = new string[100];
                            k = 0;
                            Int32 NoLoans = Convert.ToInt32(items[49]);
                            if (NoLoans > 0)
                            {
                                var EmpID = items[10];
                                var EmpAccount = items[8];
                                var EmpEIN = items[0];
                                var Details = (from b in db.AccountDatas
                                               where b.EMPLOYEE_ID == EmpID
                                                 && b.EMPLOYEE_ACCOUNT_NO == EmpAccount
                                                 && b.EMPLOYER_EIN == EmpEIN
                                                 && b.FileName == ExtFileName
                                               select b.ACCOUNTDATAID).FirstOrDefault();
                                //for (int i = 0; i < NoLoans; i++)
                                //{
                                //    //Grab the newly created record ID //
                                Loan.AccountDataID = Convert.ToInt32(Details);
                                SaveLoan(Loan);
                                //}
                            }//endif 
                        } //i

                    } //end Using
                } //try
                catch (Exception e)
                {
                    sb.AppendLine(e.Message);
                    GoodFile = 0;
                } //catch
            }//end else
            else
            {
                sb.AppendLine("Illegal File type found.");
                GoodFile = 0;
            }

            if (counter > 0)
            {
                sb.AppendLine(Convert.ToString(numbRecs) + " records found. Successfully imported " + Convert.ToString(counter) + " Records ");
                if (numbRecs == counter) //I read all of my records.
                { 
                GoodFile = 1;
                }

            }
            if (sb.Length != 0)
            {
                LogError(processType, folder, sb.ToString(), ExtFileName);
            }
            return GoodFile;
        }

        public String CheckNull(object field, string String, string FileName, string processType, string folder)
        {
            try
            {
                if (String.Length == 0)
                {
                    return "";
                }
                else
                {
                    return String;
                }
            }
            catch
            {
                LogError(processType, folder, "Field mismatch found in " + field.ToString() + " found " + String + " expected string", FileName);
                return "";
            }
        }
        public Int32? CheckInt32(object field, object String, string FileName, string processType, string folder)
        {
            try
            {
                if (String is DBNull ||
                  String == null)
                {
                    return 0;
                }
                if (String is string &&
                    ((string)String).Length == 0)
                {
                    return 0;
                }
                return Convert.ToInt32(String);
            }
            catch
            {
                LogError(processType, folder, "Field mismatch found in " + field.ToString() + " found " + String + " expected numeric", FileName);
                return 0;
            }
        }

        public decimal? CheckDecimal(object field, object Decimal, string FileName, string processType, string folder)
        {
            try
            {
                if (Decimal is DBNull ||
                Decimal == null)
                {
                    return 0;
                }
                if (Decimal is string &&
                    ((string)Decimal).Length == 0)
                {
                    return 0;
                }
                return Convert.ToDecimal(Decimal);
            }
            catch
            {
                LogError(processType, folder, "Field mismatch found in " + field.ToString() + " found " + Decimal.ToString() + " expected numeric", FileName);
                return 0;
            }
        }


        public string Save(string processType, string folder,string[] items, string FileName)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            AccountData AccountDetail = new AccountData();
            bool NullableField = false;
            string strPageField = "";
            string strPageField2 = "";
            string DBField = "";
            string temp = "items[";
            string strVal = "";
            AccountDetail.FileName = FileName;
            AccountDetail.ProcessDate = DateTime.Now.Date;
            Globals.ImportsGlobals.InitilizeMEAFSGlobals();
            // Get all the properties in Entity Class
            PropertyInfo[] Accounts = typeof(AccountData).GetProperties();
            if (AccountDetail != null)
            {
                // Scan the fields in this record. If any match the fields we've been fed
                // from the web page, fill them in.
                for (int i = 0; i < Accounts.Length; i++)
                {
                    strPageField = Accounts[i].Name;
                    strPageField2 = Accounts[i].PropertyType.ToString();
                    //counts[i].SetValue(items[i],null);
                    if (Globals.ImportsGlobals.DBFieldToPageHash.ContainsValue(strPageField))
                    {

                        temp = "items[" + i + "]";
                        DBField = (string)Globals.ImportsGlobals.DBFieldToPageHash[temp];
                        strVal = items[i];

                        System.Type coltype = Accounts[i].PropertyType;


                        if (Globals.ImportsGlobals.IsNullableType(coltype))
                        {
                            NullableField = true;
                            coltype = Nullable.GetUnderlyingType(coltype);
                        }
                        else
                        {
                            NullableField = false;
                        }
                        try
                        {
                            if (coltype == typeof(bool))
                            {
                                if (DBField == strPageField)
                                {
                                    if (string.IsNullOrEmpty(strVal))
                                    {
                                        Accounts[i].SetValue(AccountDetail, null, null);
                                    }
                                    else if ((strVal == "1") || (strVal.ToLower() == "true") || (strVal.ToLower() == "on"))
                                    {
                                        Accounts[i].SetValue(AccountDetail, true, null);
                                    }
                                    else
                                    {
                                        Accounts[i].SetValue(AccountDetail, false, null);
                                    }
                                }
                            }
                            else if (coltype == typeof(Int32))
                            {
                                if (DBField == strPageField)
                                {
                                    if (string.IsNullOrEmpty(strVal))
                                    {
                                        if (NullableField)
                                        {
                                            Accounts[i].SetValue(AccountDetail, null, null);
                                        }
                                        else
                                        {
                                            Accounts[i].SetValue(AccountDetail, 0, null);
                                        }
                                    }
                                    else
                                    {
                                        Accounts[i].SetValue(AccountDetail, Convert.ToInt32(strVal), null);
                                    }
                                }
                            }
                            else if (coltype == typeof(System.DateTime))
                            {
                                if (DBField == strPageField)
                                {
                                    if (string.IsNullOrEmpty(strVal))
                                    {
                                        if (NullableField)
                                            Accounts[i].SetValue(AccountDetail, null, null);
                                    }
                                    else
                                    {
                                        System.DateTime d = default(System.DateTime);
                                        if (System.DateTime.TryParse(strVal, out d))
                                        {
                                            Accounts[i].SetValue(AccountDetail, d, null);

                                        }
                                    }
                                }

                            }
                            else if ((coltype == typeof(char)))
                            {
                                if (DBField == strPageField)
                                {
                                    if ((string.IsNullOrEmpty(strVal)))
                                    {
                                        if ((NullableField))
                                            Accounts[i].SetValue(AccountDetail, null, null);
                                    }
                                    else
                                    {
                                        Accounts[i].SetValue(AccountDetail, strVal, null);
                                    }
                                }
                            }
                            else if (coltype == typeof(decimal))
                            {
                                if (DBField == strPageField)
                                {
                                    if ((string.IsNullOrEmpty(strVal)))
                                    {
                                        if (NullableField)
                                        {
                                            Accounts[i].SetValue(AccountDetail, null, null);
                                        }
                                        else
                                        {
                                            Accounts[i].SetValue(AccountDetail, Convert.ToDecimal(0), null);
                                        }
                                    }
                                    else
                                    {
                                        Accounts[i].SetValue(AccountDetail, Convert.ToDecimal(strVal), null);
                                    }
                                }
                            }
                            else if (DBField == strPageField)
                            {
                                Accounts[i].SetValue(AccountDetail, strVal, null);
                            }
                        }//end try
                        catch (Exception ex)
                        {
                            sb.AppendLine(Accounts[i].Name + ": Could not convert '" + strVal + "' to type " + coltype.ToString());
                        }//end catch
                    } //end if
                } //end for
                var EmpEIN = items[0];
                var vendorEIN = items[1];
                var EmpID = items[10];
                var AggPlan = items[5];
                var CashValue = items[24];

                var RecPrior = (from b in db.AccountDatas
                               where b.EMPLOYER_EIN == EmpEIN.ToString()
                                  && b.VENDOR_EIN == vendorEIN.ToString()
                                  && b.EMPLOYEE_ID == EmpID.ToString()
                                  && b.AGGREGATOR_PLAN_ID == AggPlan.ToString()
                                  && b.CASH_VALUE_DATE == CashValue.ToString()
                               select b).Count();

                var DupRecord = (from b in db.AccountDatas
                               where b.EMPLOYER_EIN == EmpEIN.ToString()
                                  && b.VENDOR_EIN == vendorEIN.ToString()
                                  && b.EMPLOYEE_ID == EmpID.ToString()
                                  && b.AGGREGATOR_PLAN_ID == AggPlan.ToString()
                                  && b.CASH_VALUE_DATE == CashValue.ToString()
                                  && (b.ProcessDate.ToString() == DbFunctions.TruncateTime(DateTime.Now).ToString())
                               select b).Count();

                if (DupRecord > 0) //Sorry I already found you Let's let the user know
                {
                    sb.AppendLine(items[11].ToString() + " " + items[12].ToString() + ": Found with possible duplicated records.");
                }
                if (RecPrior == 1) //Found them in an old file 
                {
                        var Records = db.AccountDatas.Where(u =>  u.EMPLOYER_EIN == EmpEIN.ToString()
                                  && u.VENDOR_EIN == vendorEIN.ToString()
                                  && u.EMPLOYEE_ID == EmpID.ToString()
                                  && u.AGGREGATOR_PLAN_ID == AggPlan.ToString()
                                  && u.CASH_VALUE_DATE == CashValue.ToString());

                        foreach (var u in Records)
                        {
                            db.AccountDatas.Remove(u);
                        }

                        db.SaveChanges();
                    }
                if ((DupRecord == 0) || (RecPrior < 2))  //You're fine go ahead and get added
                {
                    //sb.AppendLine(EmpEIN.ToString() + " " + vendorEIN.ToString() + " " + EmpID.ToString() + " " + Details.ToString());
                    //Duplicate data here check...
                    db.AccountDatas.Add(AccountDetail);
                    ///*Save data to database*/
                    db.SaveChanges();
                }
            } //end if 
            return sb.ToString();
        }//end function
        public int SaveLoan(LoanData _Loan)
        {

            LoanData LoanDetail = new LoanData();
            LoanDetail.AccountDataID = _Loan.AccountDataID;
            LoanDetail.VENDOR_LOAN_NUMBER = _Loan.VENDOR_LOAN_NUMBER;
            LoanDetail.LOAN_INITIATION_DATE = _Loan.LOAN_INITIATION_DATE;
            LoanDetail.LOAN_STATUS = _Loan.LOAN_STATUS;
            LoanDetail.LOAN_TYPE_INDICATOR = _Loan.LOAN_TYPE_INDICATOR;
            LoanDetail.ORIGINAL_LOAN_AMOUNT = _Loan.ORIGINAL_LOAN_AMOUNT;
            LoanDetail.REMAINING_LOAN_BALANCE = _Loan.REMAINING_LOAN_BALANCE;
            LoanDetail.REMAINING_BALANCE_DATE = _Loan.REMAINING_BALANCE_DATE;
            LoanDetail.HIGHEST_OUTSTANDING_LOAN_BALANCE_12_MONTHS = _Loan.HIGHEST_OUTSTANDING_LOAN_BALANCE_12_MONTHS;
            //LoanDetail.FileDate = DateTime.Now;
            db.LoanDatas.Add(LoanDetail);
            /*Save data to database*/
            db.SaveChanges();
            return 1;

        }

        public void LogError(string processType, string folder, string Error, string FileName)
        {
            ErrorLog errLog = new ErrorLog();
            errLog.Folder = folder;
            errLog.ProcessType = processType;
            errLog.FileName = FileName;
            errLog.Status = Error;
            db.ErrorLogs.Add(errLog);
            db.SaveChanges();
        }
    }
}
