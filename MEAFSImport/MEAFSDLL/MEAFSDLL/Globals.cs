using System;
using System.Collections;

namespace MEAFSDLL.Globals
{
    public class ImportsGlobals
    {
        public static System.Collections.Hashtable _hashPageToDBField;
        public static System.Collections.Hashtable PageToDBFieldHash
        {
            get { return _hashPageToDBField; }
        }

        private static System.Collections.Hashtable _hashDBFieldToPage;
        public static System.Collections.Hashtable DBFieldToPageHash
        {
            get { return _hashDBFieldToPage; }
        }
        public static void InitilizeMEAFSGlobals()
        {
            InitDBColumnsHashtable();
        }

        private static void InitDBColumnsHashtable()
        {
            _hashPageToDBField = new Hashtable();
            _hashPageToDBField.Add("items[0]", "EMPLOYER_EIN");
            _hashPageToDBField.Add("items[1]", "VENDOR_EIN");
            _hashPageToDBField.Add("items[2]", "VENDOR_SOURCE_NAME");
            _hashPageToDBField.Add("items[3]", "AGGREGATOR_SOURCE_NAME");
            _hashPageToDBField.Add("items[4]", "VENDOR_SOURCE_ID");
            _hashPageToDBField.Add("items[5]", "AGGREGATOR_PLAN_ID");
            _hashPageToDBField.Add("items[6]", "VENDOR_PLAN_ID");
            _hashPageToDBField.Add("items[7]", "EMPLOYER_PLAN_ID");
            _hashPageToDBField.Add("items[8]", "EMPLOYEE_ACCOUNT_NO");
            _hashPageToDBField.Add("items[9]", "EMPLOYEE_SSN");
            _hashPageToDBField.Add("items[10]", "EMPLOYEE_ID");
            _hashPageToDBField.Add("items[11]", "FIRST_NAME");
            _hashPageToDBField.Add("items[12]", "LAST_NAME");
            _hashPageToDBField.Add("items[13]", "DATE_OF_BIRTH");
            _hashPageToDBField.Add("items[14]", "CASH_VALUE_TYPE");
            _hashPageToDBField.Add("items[15]", "EMPLOYER_CASH_VALUE");
            _hashPageToDBField.Add("items[16]", "EE_DEFERRAL_CASH_VALUE");
            _hashPageToDBField.Add("items[17]", "ROLLOVER_EE_PRETAX_CASH_VALUE");
            _hashPageToDBField.Add("items[18]", "ROLLOVER_EE_POSTTAX_CASH_VALUE");
            _hashPageToDBField.Add("items[19]", "ROLLOVER_ROTH_CASH_VALUE");
            _hashPageToDBField.Add("items[20]", "EE_POSTTAX_CASH_VALUE");

            _hashPageToDBField.Add("items[21]", "ROTH_403B_CASH_VALUE");
            _hashPageToDBField.Add("items[22]", "DATE_OF_FIRST_ROTH_CONTRIBUTION");
            _hashPageToDBField.Add("items[23]", "C403B7_CASH_VALUE");
            _hashPageToDBField.Add("items[24]", "CASH_VALUE_DATE");
            _hashPageToDBField.Add("items[25]", "TYPE_OF_ACCOUNT");
            _hashPageToDBField.Add("items[26]", "YEAR_TO_DATE_EE_CONTRIBUTIONS");
            _hashPageToDBField.Add("items[27]", "YEAR_TO_DATE_ER_CONTRIBUTIONS");
            _hashPageToDBField.Add("items[28]", "ACCOUNT_INCEPTION_TO_DATE_EE_CONTRIBUTIONS");
            _hashPageToDBField.Add("items[29]", "ACCOUNT_INCEPTION_TO_DATE_15YR_CATCH_UP_CONTRIBUTIONS");
            _hashPageToDBField.Add("items[30]", "C12_31_86_CASH_VALUE_EE");
            _hashPageToDBField.Add("items[31]", "C12_31_86_CASH_VALUE_ER");
            _hashPageToDBField.Add("items[32]", "METHOD_OF_REPORTING_HARDSHIP_DATA");
            _hashPageToDBField.Add("items[33]", "TOTAL_HARDSHIP_AMOUNT_AVAILABLE");
            _hashPageToDBField.Add("items[34]", "LATEST_HARDSHIP_DISTRIBUTION_TYPE");
            _hashPageToDBField.Add("items[35]", "LATEST_HARDSHIP_DISTRIBUTION_DATE");
            _hashPageToDBField.Add("items[36]", "LATEST_HARDSHIP_DISTRIBUTION_AMOUNT");
            _hashPageToDBField.Add("items[37]", "HARDSHIP_COMPONENT_A_12_31_88_CASH_VALUE_EE");
            _hashPageToDBField.Add("items[38]", "HARDSHIP_COMPONENT_B___12_31_88_CASH_VALUE_ER");
            _hashPageToDBField.Add("items[39]", "HARDSHIP_COMPONENT_C___POST_12_31_88_CONTRIBUTIONS_EE");
            _hashPageToDBField.Add("items[40]", "HARDSHIP_COMPONENT_D___POST_12_31_88_WITHDRAWALS");
            _hashPageToDBField.Add("items[41]", "ISSUE_DATE");
            _hashPageToDBField.Add("items[42]", "C12_31_88_CASH_VALUE_EE");
            _hashPageToDBField.Add("items[43]", "METHOD_OF_FREPORTING_LOAN_DATA");
            _hashPageToDBField.Add("items[44]", "MAXIMUM_LOAN_AMOUNT_AVAILABLE");
            _hashPageToDBField.Add("items[45]", "SEPARATION_FROM_SERVICE_DATE");
            _hashPageToDBField.Add("items[46]", "NUMBER_OF_LOANS_OUTSTANDING");
            _hashPageToDBField.Add("items[47]", "PRODUCT_ID");
            _hashPageToDBField.Add("items[48]", "LOAN_DEFAULT_LOAN_INDICATOR");
            _hashPageToDBField.Add("items[49]", "NUMBER_OF_SETS_OF_LOAN_COMPONENT_DATA_REPORTED");
            _hashDBFieldToPage = new Hashtable(_hashPageToDBField.Count);
            foreach (DictionaryEntry kvp in _hashPageToDBField)
            {
                _hashDBFieldToPage[kvp.Key] = kvp.Value ;
            }
        }


        public static bool IsNullableType(Type myType)
        {
            return (myType.IsGenericType) && (object.ReferenceEquals(myType.GetGenericTypeDefinition(), typeof(Nullable<>)));
        }
    }
}